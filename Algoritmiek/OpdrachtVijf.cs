﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmiek
{
    internal class OpdrachtVijf
    {
        /***
         * INPUT VOOR OPGAVE 5 EXTRA:
         * 1 2 -1 3 -2 4 -3 5 -4 6 -5 7 -6 8 -7 9 -8 10 -9 0
         */
        public void Start()
        {
            List<int> Nums = new List<int>();
            int x = int.Parse(Console.ReadLine());

            while (x != 0)
            {
                if (x > 0)
                {
                    for (int i = 0; i < x; i++)
                    {
                        Nums.Add(x);
                    }
                }
                else if (x < 0)
                {
                    //Must multiply by -1 to make a positive num
                    int FromIndex = Nums.Count - (x * -1);
                    int AmountToDelete = (x * -1);

                    Nums.RemoveRange(FromIndex, AmountToDelete);
                }
                else if (x == 0)
                {
                    break;
                }
                x = int.Parse(Console.ReadLine());
            }
            PrintProgress(Nums);
        }
        public void PrintProgress(List<int> Array)
        {
            Console.WriteLine($"Aantal elementen is {Array.Count}");
            Console.WriteLine($"Laatste element is {Array[Array.Count - 1]}");

            Console.Write($"De lijst is: ");
            foreach (int i in Array)
            {
                Console.Write(i + " ");
            }
        }
    }
}
