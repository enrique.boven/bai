﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmiek
{
    internal class OpdrachtDrie
    {
        public void Start()
        {
            int[] Nums = Util.GetRandomNums(10, 30);

            Console.Write("Current Array: ");
            Util.PrintIntegerArray(Nums);

            Console.Write($"\nSmallest number is: {Util.FindLowestNumInArray(Nums)}");
            Console.Write($"\nSecond Smallest number is: {Util.FindSecondLowestNumInArray(Nums)}");
        }
    }
}
