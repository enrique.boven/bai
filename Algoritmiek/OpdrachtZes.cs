﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmiek
{
    internal class OpdrachtZes
    {

        List<int> Nums = new List<int>();
        public void Start()
        {
            
            for(int i = 5; i < 100; i += 5)
            {
                Nums.Add(i);
            }
            Console.WriteLine("De lijst is:");
            PrintList();
            for (int i = 0; i < Nums.Count; i++)
            {
                Nums.RemoveAt(i);
            }
            Console.WriteLine("\nDe lijst na verwijdering van de even posities:");
            PrintList();
        }

        public void PrintList() { 
            foreach(int num in Nums)
            {
                Console.Write($"{num} ");
            }
        }
    }
}
