﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmiek
{
    internal class Util
    {

        private static Random Random = new Random();

        public static int[] GetRandomNums(int Amount, int MaxNum)
        {
            int[] List = new int[Amount];
            for (int i = 0; i < Amount; i++)
            {
                List[i] = Random.Next(MaxNum);
            }
            return List;
        }

        public static int FindHighestNumInArray(int[] Array)
        {
            int max = 0;
            for(int i = 0; i < Array.Length; i++)
            {
                if (Array[i] > max)
                {
                    max = Array[i];
                }
            }
            return max;
        }

        public static int FindLowestNumInArray(int[] Array)
        {
            int Smallest = int.MaxValue;
            for(int i = 0; i < Array.Length; i++)
            {
                if(Array[i] < Smallest)
                {
                    Smallest = Array[i];
                }
            }
            return Smallest;
        }

        public static int FindSecondLowestNumInArray(int[] Array)
        {
            int Smallest = FindLowestNumInArray(Array);
            int SecondSmallest = int.MaxValue;

            for (int i = 0; i < Array.Length; i++)
            {
                if (Array[i] < SecondSmallest && Array[i] > Smallest)
                {
                    SecondSmallest = Array[i];
                }
            }
            return SecondSmallest;
        }

        public static int SumUp(int Min, int Max)
        {
            int total = 0;
            for (int i = Min; i <= Max; i++)
                {
                total += i;
                }
            return total;
        }

        public static void PrintIntegerArray(int[] Array)
        {
            foreach(int x in Array)
            {
                Console.Write(x + " ");
            }
        }
    }
}
