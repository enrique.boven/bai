﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Algoritmiek
{
    internal class OpdrachtTwee
    {
        public void Start()
        {
            
            int[] Nums = Util.GetRandomNums(10, 100);
            Util.PrintIntegerArray(Nums);
            Console.WriteLine($"\nHighest number in this array is: {Util.FindHighestNumInArray(Nums)}");
            
        }
    }
}
